export class User {
    name: string;
    email: string;
    password: string;
    _role: string;

    constructor( data: any ) {
        this.name = data.name;
        this.email = data.email;
        this.password = data.password;
        this._role = data._role;
    }
}
