enum Genre {
    'pop',
    'rock',
    'jazz'
}

export class Album {
    _id: string;
    name: string;
    year: number;
    genre: Genre;
    cover: string;
    _artist: string;
    _collaborators: string[];

    constructor( data: any ) {
        if ( data._id ) {
            this._id = data._id;
        }
        this.name = data.name;
        this.year = data.year;
        this.genre = data.genre;
        this.cover = data.cover;
        this._artist = data._artist;
        this._collaborators = data._collaborators;
    }
}
