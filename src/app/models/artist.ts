export class Artist {
    _id: string;
    name: string;
    country: string;
    bio: string;
    grammys: number;
    photo: string;

    constructor( data: any ) {
        if ( data._id ) {
            this._id = data._id;
        }
        this.name = data.name;
        this.country = data.country;
        this.bio = data.bio;
        this.grammys = data.grammys;
        this.photo = data.photo;
    }

}
