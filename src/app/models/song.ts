export class Song {
    _id: string;
    name: string;
    filename: string;
    length: string;
    _album: string;

    constructor( data?: any ) {
        if ( data ) {
            this._id = data._id;
            this.name = data.name;
            this.filename = data.filename;
            this.length = data.length;
            this._album = data._album;
        }
    }
}
