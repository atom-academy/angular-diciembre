import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  


  loginForm = this.fb.group({
    email: [''],
    password: ['']
  });

  constructor(
    public loginService: LoginService,
    private fb: FormBuilder,
    private router: Router
  ) {
    console.log( this.loginService );
  }

  ngOnInit() {
  }

  doLogin(){
    
    console.log( this.loginForm.value );
    
    
    // Aquí hago el submit
    this.loginService.login( this.loginForm.value ).subscribe(
      data => {
        if ( data.token ) {
          // Almacenar token en local storage 
          this.loginService.saveUserdata( data );
          console.log('se ha almacenado el token');
          this.router.navigate(['/']);
          this.loginService.message('Bienvenido');
          this.loginService.validate_token();
          // Debería redireccionar al homepage
        } else {
          this.loginForm.patchValue({
            pass: ''
          });
          alert('Email o password incorrectos');
        }
        
      },
      err => {
        console.log('error', err);
        alert('El email o contraseña son incorrectos');
        this.loginForm.patchValue({
          pass: ''
        });
      }
    )
    
    
  }

  prop( item:string ) { return this.loginForm.get(item); }

}
