import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm = this.fb.group({
    name: [''],
    email: [''],
    password: [''],
    confirm_password: ['']
  });

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService
  ) { }

  ngOnInit() {
  }

  doRegister() {
    
    this.loginService.register( this.registerForm.value ).subscribe(
      data => {
        console.log( data );
        alert('Usuario creado satisfactoriamente');
      },
      error => {
        console.log( error );
        alert('Error creando usuario');
      }
    );
  }

  prop( item:string ) { return this.registerForm.get(item); }

}
