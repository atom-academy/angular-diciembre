import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';

import { AngularMaterialModule } from 'src/app/common/angular-material.module';
import { ArtistNewEditComponent } from './artist-new-edit/artist-new-edit.component';
import { ArtistComponent } from './artist/artist.component';
import { AlbumNewEditComponent } from './artist/album-new-edit/album-new-edit.component';

import { ArtistService } from 'src/app/services/artist.service';
import { AlbumComponent } from './artist/album/album.component';

@NgModule({
  declarations: [HomeComponent, ArtistNewEditComponent, ArtistComponent, AlbumNewEditComponent, AlbumComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    AngularMaterialModule
  ],
  providers: [ ArtistService ],
  entryComponents: [ ArtistNewEditComponent, AlbumNewEditComponent ]
})
export class HomeModule { }
