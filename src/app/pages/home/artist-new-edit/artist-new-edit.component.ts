import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Artist } from 'src/app/models/artist';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-artist-new-edit',
  templateUrl: './artist-new-edit.component.html',
  styleUrls: ['./artist-new-edit.component.scss']
})
export class ArtistNewEditComponent implements OnInit {

  artistForm = this.fb.group({
    _id: [''],
    name: [''],
    country: [''],
    bio: [''],
    grammys: [''],
    photo: ['']
  });

  dialogTitle: string = 'Nuevo artista';

  constructor(
    private fb: FormBuilder,
    public loginService: LoginService,
    @Inject(MAT_DIALOG_DATA) public data: Artist
  ) { }

  ngOnInit() {
    console.log( this.data );
    if ( this.data && this.data._id ) {
      this.dialogTitle = 'Editar artista';
      Object.keys( this.data ).forEach( key => {
        if ( this.artistForm.get(key) ) {
          this.artistForm.get(key).setValue( this.data[key] );
        }
      });
    }
  }

  changeAvatar( ev: any ) {
    this.convertTobase64( ev.target );
  }

  convertTobase64(inputValue: any): void {
    // Guarda la imagen seleccionada en una variable llamada "file"
    var file:File = inputValue.files[0];
    // Valida que la imagen sea formato JPG o PNG
    if (file.type === 'image/jpeg' || file.type === 'image/jpg' || file.type === 'image/png' )
    {
      // Crea una variable que nos ayuda a leer archivos (una instancia de clase)
      var myReader:FileReader = new FileReader();
      // A nuestro lector de archivos, le pedimos leer en formato Base64
      myReader.readAsDataURL(file);
      // Cuando termina la lectura del archivo, el resultado está en myReader.result
      myReader.onloadend = (e) => {
        // El resultado es asignado como valor a la propiedad del formulario
        this.artistForm.get('photo').setValue( myReader.result);
      }
      
    }else{
      alert('Solo se permiten jpg y png');
    }

  }

  


}
