import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtistNewEditComponent } from './artist-new-edit.component';

describe('ArtistNewEditComponent', () => {
  let component: ArtistNewEditComponent;
  let fixture: ComponentFixture<ArtistNewEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistNewEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistNewEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
