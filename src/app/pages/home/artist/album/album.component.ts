import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

import { ArtistService } from 'src/app/services/artist.service';
import { LoginService } from 'src/app/services/login.service';

import { Artist } from 'src/app/models/artist';
import { Album } from 'src/app/models/album';
import { Song } from 'src/app/models/song';

import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit {

  artist_id: string;
  album_id: string;

  artist: Artist;
  album: Album;
  songs: Song[];

  new_song: Song = new Song();
  song_file: any;

  constructor(
    private route: ActivatedRoute,
    private artistService: ArtistService,
    public loginService: LoginService
  ) { }

  ngOnInit() {
    this.artist_id = this.route.snapshot.paramMap.get("id_artist");
    this.album_id = this.route.snapshot.paramMap.get("id");
    
    this.artistService.getArtist( this.artist_id ).subscribe(
      data => {
        this.artist = data;
        console.log( this.artist );
      },
      err => {
        this.loginService.message('Ha ocurrido un error al buscar el artista')
      }
    );

    this.artistService.getAlbum( this.artist_id, this.album_id ).subscribe(
      data => {
        this.album = data;
        console.log( this.album );
      },
      err => {
        this.loginService.message('Ha ocurrido un error al buscar el album')
      }
    );

    this.artistService.getAlbumSongs( this.album_id ).subscribe(
      data => {
        this.songs = data;
        console.log( this.songs );
      },
      err => {
        this.loginService.message('Ha ocurrido un error al buscar las canciones')
      }
    );
  }

  changeSongFile( ev: any ) {
    this.song_file = ev.target.files[0];
  }

  showNames( colaboradores: any[], prop: string ) {
    return colaboradores.map( el => el[prop] );
  }

  saveSong() {
    this.artistService.saveSong( this.album_id, this.new_song, this.song_file ).subscribe(
      data => {
        if ( data && data._id ) {
          this.songs.push( data );
          this.new_song = new Song();
          this.song_file = null;
        } else {
          this.loginService.message('Ha ocurrido un error al guardar la canción')
        }
      },
      err => {
        this.loginService.message('Ha ocurrido un error al guardar la canción')
      }
    );
  }

  playSong( song: Song ) {

    var url = environment.apiUrl + '/album/'+song._album+'/song/'+song._id+'/file';
    this.loginService.audioControl.file = null;
    setTimeout( () => {
      this.loginService.audioControl.file = url;
      this.loginService.audioControl.name = song.name;
    }, 10);
  }

  

}
