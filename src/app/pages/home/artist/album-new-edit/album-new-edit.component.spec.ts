import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumNewEditComponent } from './album-new-edit.component';

describe('AlbumNewEditComponent', () => {
  let component: AlbumNewEditComponent;
  let fixture: ComponentFixture<AlbumNewEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbumNewEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumNewEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
