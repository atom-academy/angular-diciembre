import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Album } from 'src/app/models/album';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-album-new-edit',
  templateUrl: './album-new-edit.component.html',
  styleUrls: ['./album-new-edit.component.scss']
})
export class AlbumNewEditComponent implements OnInit {

  genreOptions: string[] = ['pop','rock','jazz']

  albumForm = this.fb.group({
    _id: [''],
    name: [''],
    year: [''],
    genre: [[]],
    cover: ['']
  });

  dialogTitle: string = 'Nuevo album';

  constructor(
    private fb: FormBuilder,
    public loginService: LoginService,
    @Inject(MAT_DIALOG_DATA) public data: Album
  ) { }

  ngOnInit() {
    console.log( this.data );
    if ( this.data && this.data._id ) {
      this.dialogTitle = 'Editar album';
      Object.keys( this.data ).forEach( key => {
        if ( this.albumForm.get(key) ) {
          this.albumForm.get(key).setValue( this.data[key] );
        }
      });
    }
  }

  changeAvatar( ev: any ) {
    this.convertTobase64( ev.target );
  }

  convertTobase64(inputValue: any): void {
    // Guarda la imagen seleccionada en una variable llamada "file"
    var file:File = inputValue.files[0];
    // Valida que la imagen sea formato JPG o PNG
    if (file.type === 'image/jpeg' || file.type === 'image/jpg' || file.type === 'image/png' )
    {
      // Crea una variable que nos ayuda a leer archivos (una instancia de clase)
      var myReader:FileReader = new FileReader();
      // A nuestro lector de archivos, le pedimos leer en formato Base64
      myReader.readAsDataURL(file);
      // Cuando termina la lectura del archivo, el resultado está en myReader.result
      myReader.onloadend = (e) => {
        // El resultado es asignado como valor a la propiedad del formulario
        this.albumForm.get('cover').setValue( myReader.result);
      }
      
    }else{
      alert('Solo se permiten jpg y png');
    }

  }
  

  


}
