import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import {MatDialog} from '@angular/material/dialog';
import { ArtistService } from 'src/app/services/artist.service';
import { LoginService } from 'src/app/services/login.service';
import { Artist } from 'src/app/models/artist';
import { Album } from 'src/app/models/album';
import { AlbumNewEditComponent } from './album-new-edit/album-new-edit.component';

import * as _ from 'lodash';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.scss']
})
export class ArtistComponent implements OnInit {

  artist_id: string;
  artist: Artist;
  albums: Album[];

  sortBy: string = '';

  constructor(
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private artistService: ArtistService,
    public loginService: LoginService
  ) { }

  ngOnInit() {
    this.artist_id = this.route.snapshot.paramMap.get("id");

    // Get artist info
    this.artistService.getArtist( this.artist_id ).subscribe(
      data => {
        this.artist = data;
      },
      err => {
        this.loginService.message('Ha ocurrido un error al buscar el artista')
      }
    );
    
    // Get albums
    this.artistService.getAllAlbums( this.artist_id ).subscribe(
      data => {
        this.albums = data;
      },
      err => {
        this.loginService.message('No se ha encontrado la información');
      }
    )
  }

  sortItems() {
    this.albums = _.orderBy(this.albums, [this.sortBy], ['asc']);
    if ( this.sortBy == '_id' ) {
      this.albums.reverse();
    }
  }

  newAlbumDialog(): void {
    const dialogRef = this.dialog.open(AlbumNewEditComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if ( result ) {
        
        // Save album
        this.artistService.createAlbum( this.artist_id, result ).subscribe(
          data => {
            if ( data && data._id ) {
              this.loginService.message('El album ha sido creado');
              this.albums.push( data );
            } else {
              this.loginService.message('Ha ocurrido un error al guardar el album');
            }
          },
          err => {
            this.loginService.message('Ha ocurrido un error al guardar el album');
          }
        );
        
      }
      
    });
  }

  editAlbum( alb: Album ) {
    const dialogRef = this.dialog.open(AlbumNewEditComponent, {
      width: '400px',
      data: alb
    });

    dialogRef.afterClosed().subscribe(result => {
      if ( result ) {
        // Save artist
        this.artistService.updateAlbum( this.artist_id, alb._id, result ).subscribe(
          data => {
            if ( data && data._id ) {
              this.loginService.message('El album ha sido actualizado');
              //this.artists.push( data );
              let index = this.albums.findIndex( el => el._id == alb._id );
              this.albums[ index ] = data;
            } else {
              this.loginService.message('Ha ocurrido un error al guardar el album');
            }
          },
          err => {
            this.loginService.message('Ha ocurrido un error al guardar el album');
          }
        );
      }
      
    });
  }

  removeAlbum( alb: Album ) {
    if ( confirm('¿Deseas borrar el album?') ) {
      this.artistService.deleteAlbum( this.artist_id, alb._id ).subscribe(
        data => {
          let index = this.albums.findIndex( el => el._id == alb._id );
          this.albums.splice(index,1);
          this.loginService.message('El album ha sido borrado');
        },
        err => {
          this.loginService.message('Ha ocurrido un error al borrar el album');
        }
      )
    }
  }

}
