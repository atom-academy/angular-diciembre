import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';

import { LoginService } from 'src/app/services/login.service';

import { ArtistService } from 'src/app/services/artist.service';
import { Artist } from 'src/app/models/artist';

import { environment } from 'src/environments/environment';

import { ArtistNewEditComponent } from './artist-new-edit/artist-new-edit.component'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  artists: Artist[];

  uploadsUrl: string = environment.uploadsUrl;

  constructor(
    public dialog: MatDialog,
    public loginService: LoginService,
    private artistService: ArtistService
  ) { }

  ngOnInit() {
    
    //console.log( 'home component', this.loginService )
    // Aquí cargo el listado de artistas
    this.artistService.getAll().subscribe(
      data => {
        this.artists = data;
        console.log( this.artists );
      },
      err => {
        console.log('error', err);
      }
    )
  }

  getArtistImage( art: Artist) {
    if ( art.photo ) {
      return this.uploadsUrl + art.photo;
    } else {
      return this.uploadsUrl + 'default.jpg';
    }
    
  }

  newArtistDialog(): void {
    const dialogRef = this.dialog.open(ArtistNewEditComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if ( result ) {
        // Save artist
        this.artistService.createArtist( result ).subscribe(
          data => {
            if ( data && data._id ) {
              this.loginService.message('El artista ha sido creado');
              this.artists.push( data );
            } else {
              this.loginService.message('Ha ocurrido un error al guardar el artista');
            }
          },
          err => {
            this.loginService.message('Ha ocurrido un error al guardar el artista');
          }
        );
      }
      
    });
  }

  editArtist( art: Artist ) {
    const dialogRef = this.dialog.open(ArtistNewEditComponent, {
      width: '400px',
      data: art
    });

    dialogRef.afterClosed().subscribe(result => {
      if ( result ) {
        // Save artist
        this.artistService.updateArtist( art._id, result ).subscribe(
          data => {
            if ( data && data._id ) {
              this.loginService.message('El artista ha sido actualizado');
              //this.artists.push( data );
              let index = this.artists.findIndex( el => el._id == art._id );
              this.artists[ index ] = data;
            } else {
              this.loginService.message('Ha ocurrido un error al guardar el artista');
            }
          },
          err => {
            this.loginService.message('Ha ocurrido un error al guardar el artista');
          }
        );
      }
      
    });
  }

  removeArtist( art: Artist ) {
    if ( confirm('¿Deseas borrar el artista?') ) {
      this.artistService.deleteArtist( art._id ).subscribe(
        data => {
          let index = this.artists.findIndex( el => el._id == art._id );
          this.artists.splice(index,1);
          this.loginService.message('El artista ha sido borrado');
        },
        err => {
          this.loginService.message('Ha ocurrido un error al borrar el artista');
        }
      )
    }
  }

}
