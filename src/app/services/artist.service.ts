import { Injectable } from '@angular/core';
import { Artist } from 'src/app/models/artist';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { LoginService } from 'src/app/services/login.service';
import { Album } from '../models/album';
import { Song } from '../models/song';

@Injectable({
  providedIn: 'root'
})
export class ArtistService {

  constructor(
    private http: HttpClient,
    private loginService: LoginService
  ) { }
  

  // Artists
  getAll() : Observable<Artist[]> {
    return this.http.get<Artist[]>(environment.apiUrl+'artists').pipe( map( convertData ) );
  }

  getArtist( id_artist: string ) : Observable<Artist> {
    return this.http.get<Artist>(environment.apiUrl+'artist/'+id_artist).pipe( map( convertData ) );
  }

  createArtist( artist: Artist ) : Observable<Artist> {
    return this.http.post<Artist>(environment.apiUrl+'artist', artist, this.loginService._requestOptions() ).pipe( map( convertData ) );
  }

  updateArtist( id_artist: string, artist: Artist ) : Observable<Artist> {
    return this.http.patch<Artist>(`${environment.apiUrl}artist/${id_artist}`, artist, this.loginService._requestOptions() ).pipe( map( convertData ) );
  }

  deleteArtist( id_artist: string ) : Observable<Artist> {
    return this.http.delete<Artist>(environment.apiUrl+'artist/'+id_artist, this.loginService._requestOptions()).pipe( map(convertData) );
  }

  // Albums
  getAllAlbums( id_artist: string ) : Observable<Album[]> {
    return this.http.get<Album[]>(environment.apiUrl+'artist/'+id_artist+'/albums').pipe( map( convertData ) );
  }

  getAlbum( id_artist: string, id_album: string ) : Observable<Album> {
    return this.http.get<Album>(environment.apiUrl+'artist/'+id_artist+'/album/'+id_album).pipe( map( convertData ) );
  }

  createAlbum( id_artist: string, album: Album ) : Observable<Album> {
    return this.http.post<Album>(environment.apiUrl+'artist/'+id_artist+'/album', album, this.loginService._requestOptions() ).pipe( map( convertData ) );
  }

  updateAlbum( id_artist: string, id_album: string, album: Album ) : Observable<Album> {
    return this.http.patch<Album>(`${environment.apiUrl}artist/${id_artist}/album/${id_album}`, album, this.loginService._requestOptions() ).pipe( map( convertData ) );
  }

  deleteAlbum( id_artist: string, id_album: string ) : Observable<Album> {
    return this.http.delete<Album>(environment.apiUrl+'artist/'+id_artist+'/album/'+id_album, this.loginService._requestOptions()).pipe( map(convertData) );
  }

  // Songs
  getAlbumSongs( id_album: string ) : Observable<Song[]> {
    return this.http.get<Song[]>(environment.apiUrl+'/album/'+id_album+'/songs').pipe( map( convertData ) );
  }

  saveSong( id_album: string, song: Song, songFile: any ) : Observable<Song> {
    var formData = new FormData();
    formData.append("song", songFile);
    formData.append("name", song.name);
    return this.http.post<Song>(environment.apiUrl+'album/'+id_album+'/song', formData, this.loginService._requestOptions() ).pipe( map( convertData ) );
  }


}

function convertData( data: any ) {
  if ( data.data )
    return data.data;
  return data;
}
