import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from 'src/app/models/user';


import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  token: string;
  user: User;

  audioControl: any = {
    file: '',
    name: ''
  }

  constructor(
    private http: HttpClient,
    private _snackBar: MatSnackBar
  ) {
    var tmp_token = localStorage.getItem('token');
    if ( tmp_token ) {
      // Aquí se debe validar el token
      this.token = tmp_token;
      this.validate_token();
    }
  }

  _requestOptions() {
    let headers = new HttpHeaders({'Authorization': this.token});
    let requestOptions = {headers : headers};
    return requestOptions;
  }

  validate_token() {
    this.http.get(environment.apiUrl+'auth/validate-token',this._requestOptions()).pipe( map( convertData ) ).subscribe(
      data => {
        if ( data && data._id ) {
          this.user = data;
        } else {
          localStorage.clear();
          this.token = null;
        }
      },
      err => {
        localStorage.clear();
        this.token = null;
        console.log('error', err);
      }
    )
  }

  hasPermission( permission: string ){
    if ( this.user && this.user._role && this.user._role['_permissions'] ) {
      let item = this.user._role['_permissions'].find( el => {
        return el.name == permission;
      });
      if ( item ) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  message( msg: string ) {
    this._snackBar.open( msg, 'Cerrar', { duration: 3000 } );
  }

  imagePath( img?: string, def?: string ) {
    if ( !img ) {
      img = def;
    }
    if ( img.indexOf('data') == 0 ) {
      return img;
    }
    return environment.uploadsUrl + img;
  }

  

  login( auth: any ) : Observable<any> {
    return this.http.post(environment.apiUrl+'auth/login', auth);
  }

  register( user: User ) : Observable<User> {
    return this.http.post<User>(environment.apiUrl+'auth/register', user).pipe( map( convertData ) );
  }

  saveUserdata( data: any ) {
    localStorage.setItem('token', data.token);
    this.token = data.token;
    this.user = data.data;
  }

  logout(){
    localStorage.clear()
    this.token = null;
    this.user = null;
  }

  

}

function convertData( data: any ) {
  if ( data.data )
    return data.data;
  return data;
}
